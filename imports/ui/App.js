import React from 'react'
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'

import LoadingIndicator from "./LoadingIndicator"
import PostForm from './PostForm'

const App = ({ loading, posts = [] }) =>
  <div className="app">
    <LoadingIndicator status={loading} msg="Loading posts"/>
    <div className="posts">
      {
        posts.map(post =>
          <div className="post" key={post._id}>
            <hr/>
            <h1>{post.title}</h1>
            <p>{post.body}</p>
          </div>
        )
      }
    </div>
    <PostForm/>
  </div>

const postsQuery = gql`
  query posts {
    posts {
      _id
      title
      body
    }
  }
`

export default graphql(postsQuery, {
  props: ({ data }) => ({ ...data })
})(App)