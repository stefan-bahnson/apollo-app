import React from 'react';

const LoadingIndicator = ({ status = false, msg = "Loading" }) => {
  const styles = {
    opacity: status ? 1 : 0,
    transform: `translateX(${status ? 0 : "20px"})`,
    transition: "all 0.5s 0.5s"
  }

  return (
    <div className="loading-indicator" style={styles}>{msg}</div>
  )
}

export default LoadingIndicator;