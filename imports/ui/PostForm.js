import React, { Component } from 'react'
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

class PostForm extends Component {
  render() {
    return (
      <div className="post-form">
        <label htmlFor="title">Title</label>
        <input type="text" name="title" ref={input => this.title = input}/>
        <label htmlFor="body">Body</label>
        <input type="text" name="body" ref={input => this.body = input}/>
        <button type="submit" onClick={() => this.submitForm()}>Create Post</button>
      </div>
    )
  }

  submitForm = () => {
    const formData = {
      title: this.title.value,
      body: this.body.value,
    }

    console.log(formData);

    this.props
      .createPost({
        variables: {
          title: this.title.value,
          body: this.body.value
        }
      })
      .then(res => console.log(res))
      .catch(err => console.error(err))

  }
}

const createPost = gql`
  mutation createPost($title: String!, $body: String!) {
    createPost(title: $title, body: $body) {
      _id
      title
      body
    }
  }
`

export default graphql(createPost, { 
  name: "createPost",
  options: {
    refetchQueries: ["posts"]
  }
 })(PostForm)
