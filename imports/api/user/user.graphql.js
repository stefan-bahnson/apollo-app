import gql from 'graphql-tag';

export default gql`
  type User {
    name: String
  }

  extend type Query {
    users: [User]
  }

  extend type Mutation {
    createUser(name: String!): User
  }
`