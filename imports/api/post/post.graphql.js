import gql from 'graphql-tag';

export default gql`
type Post {
  _id: String!
  title: String!
  body: String!
}

type Query {
  posts: [Post]
  post(_id: String!): Post
  postsCount: Int!
}

type Mutation {
  createPost(title: String! body: String): Post
}
`