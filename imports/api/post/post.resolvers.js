import Posts from './post.collection';

const post = {
  _id: "asd",
  title: "asdddd",
  body: "nngrkrng"
}

export default {
  Query: {
    posts: () => {
      return Posts.find().fetch()
    },
    post: (_, {_id}) => {
      return Posts.findOne({ _id })
    },
    postsCount: () => {
      return Posts.find().count()
    },
  },

  Mutation: {
    createPost(obj, args, context) {
      const _id = Posts.insert(args)
      return Posts.findOne({ _id })
    },
  }
}