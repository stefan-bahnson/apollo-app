import { createApolloServer } from 'meteor/apollo';
import { makeExecutableSchema } from 'graphql-tools';
import merge from 'lodash/merge';

import PostSchema from '../../api/post/post.graphql.js';
import PostResolvers from '../../api/post/post.resolvers';

import UserSchema from '../../api/user/user.graphql.js'
import UserResolvers from '../../api/user/user.resolvers.js'
// hi
const typeDefs = [
  PostSchema,
  UserSchema,
]

const resolvers = merge(
  PostResolvers,
  UserResolvers,
)

const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
})

createApolloServer({ schema })