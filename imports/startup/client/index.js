import { Meteor } from 'meteor/meteor';
import React from 'react';
import { render } from 'react-dom';
import { ApolloProvider } from 'react-apollo';
import { ApolloClient } from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';

import App from '../../ui/App';

// setup graphql client
const link = new HttpLink({
  uri: Meteor.absoluteUrl('graphql')
})
const cache = new InMemoryCache()
const client = new ApolloClient({ link, cache })


Meteor.startup(() => {
  render(
    <ApolloProvider client={client}>
      <App/>
    </ApolloProvider>,
    document.getElementById('app'))
})